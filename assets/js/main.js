$(document).ready(function () {


    // Dropdown Menu
    if ($(window).width() < 1024) {
        function dropDownMenu() {

            $(".menu_list > .menu_item.has_dropdown").on('click', function () {
                $(this).toggleClass('active');
            });
        }
    }

    $(window).on('load', function () {
        dropDownMenu();
    });

    // $('.menu_list > .menu_item').mouseenter(function () {
    //     $(this).toggleClass('active');
    // });

    // Burger menu

    $('.burger_menu').on('click', function () {
            $('.burger_menu, .header').toggleClass('active');

            $('body').toggleClass('lock');
        }
    );

    // Scroll Mobile Menu

    function scrollMenu() {
        var mobMenu = $('.wrap_mob');
        var windowHeight = $(window).height();
        if ($(window).width() <= 880) {
            $(mobMenu).css("height", windowHeight);
        } else if ($(window).width() >= 880) {
            $(mobMenu).css('height', 'auto');
        }
    }

    $(window).on('load resize', function () {
        scrollMenu();
    });

    // Close Menu
    var width = $(window).width();
    if (width <= 880) {
        var wrapMob = $('.wrap_mob');
        var burgerBtn = $('.burger_menu');
        $(document).on('mouseup', function (e) {
            if (!wrapMob.is(e.target) && wrapMob.has(e.target).length === 0 && !burgerBtn.is(e.target) ) {
                $('.burger_menu, .header').removeClass('active');
                $('body').removeClass('lock');
            }
        });
    }

    // Play video

    $('.play_icon').on('click', function () {
        $(this).siblings().trigger('click');
    });

    // Модальное окно fancybox

    $(".modal").fancybox({
        wrapCSS: 'call_back',
        padding: 10,
        autoFocus: false,
    });

    $(".about").fancybox({
        wrapCSS: 'about_course',
        padding: 10,
        autoFocus: false,
    });

    // Закрытие текущего модального окна перед открытием нового

    $.fancybox.defaults.closeExisting = true;


    // Show Password
    $('body').on('click', '.password-control', function () {
        if ($('#password').attr('type') == 'password') {
            $(this).addClass('view');
            $('#password').attr('type', 'text');
        } else {
            $(this).removeClass('view');
            $('#password').attr('type', 'password');
        }
        return false;
    });

    // Табы сайдбара

    $('.acc-body').hide();

    function f_acc() {

        if ($(this).hasClass('open')) {
            $(this).removeClass('open');

        } else {
            $('.grammar_lesson .lesson_head').removeClass('open');
            $(this).addClass('open');
        }
        $('.grammar_lesson .lesson_body').not($(this).next()).slideUp(500);
        $(this).next().slideToggle(500);
    }

    $(document).ready(function () {

        //прикрепляем клик по заголовкам acc-head
        $('.grammar_lesson .lesson_head').on('click', f_acc);
    });


    /* Test Diagram */

    var circle = function (sel) {
        var circles = document.querySelectorAll(sel);
        [].forEach.call(circles, function (el) {
            var valEl = parseFloat(el.innerHTML);
            var valElSpan = parseFloat(el.innerHTML);
            valEl = valEl * 408 / 100;
            el.innerHTML = '<div class="percent"> ' + valElSpan + ' <span>%</span></div><svg width="160" height="160"><circle transform="rotate(-90)" r="65" cx="-80" cy="80" /><circle transform="rotate(-90)" style="stroke-dasharray:' + valEl + 'px 408px;" r="65" cx="-80" cy="80" /></svg>';

        });
    };
    circle('.circle');

});

